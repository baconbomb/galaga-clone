package Galaga;

import java.util.ArrayList;
import java.util.Iterator;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class DeadState extends BasicGameState{

	/*
	 * Players ship was destroyed
	 * state will waiting to respawn ship
	 */
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {		
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		GalagaGame gg = (GalagaGame) game;
				
		gg.lives -= 1;			//remove a life
		gg.ship = null;			//no player ship
	}

	//same as playing state, but without player control
	//enemies will still spawn and fly around while waiting for the players ship to respawn
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		for(Background b : gg.bg) {
			b.render(g);
		}
		
		for(ArrayList<EnemyShip> ES : gg.enemies) {
			for(EnemyShip e : ES) {
				if(!e.xFinalPos) {
					gg.rotate =  e.velocity.getRotation() - 90;
					g.rotate(e.getX(), e.getY(), (float) gg.rotate);
					e.render(g);
					g.rotate(e.getX(), e.getY(), (float) -gg.rotate);
				}
				else {
					e.render(g);
				}

			}
		}
		for(TractorBeamEnemy t : gg.tractorEnemy) {
			t.render(g);
		}
		
		
		for(EnemyBullet eb : gg.enemyBullets) {
			gg.rotate =  eb.velocity.getRotation() - 90;
			g.rotate(eb.getX(), eb.getY(), (float) gg.rotate);
			eb.render(g);
			g.rotate(eb.getX(), eb.getY(), (float) -gg.rotate);
		}
		for(Explosion e : gg.explosions) {
			e.render(g);
		}
		
		g.scale(2, 2);
		g.setColor(Color.red);
		g.drawString("Lives " + gg.lives, 10, 10);
		g.drawString("Score : " + gg.score, 170, 10);
	}

	//same as playing state
	//no player ship
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		GalagaGame gg = (GalagaGame)game;
		
		gg.respawnTimer = gg.respawnTimer + delta;
		if(gg.respawnTimer > 1500) {
			gg.respawnTimer = 0;
			gg.ship = new ArrayList<Ship>();
			gg.ship.add(new Ship(GalagaGame.screenwidth / 2, GalagaGame.screenheight - 50, 0f, 0f));
			game.enterState(GalagaGame.PLAYINGSTATE);
		}
		
		gg.gameTimer = gg.gameTimer + delta;
		gg.moveTimer = gg.moveTimer + delta;	
		
		if(gg.moveTimer > 500) {
			gg.moveTimer = 0;
			gg.moveDirection = gg.moveDirection + gg.moveAmount;
			if(gg.moveDirection > 30) {
				gg.moveAmount = gg.moveAmount * -1;
			}
			if(gg.moveDirection < 0) {
				gg.moveAmount = gg.moveAmount * -1;
			}
		}
		
		if(gg.spawnNumber < gg.spawnTimers.length ) {				//still enemies to spaw
			if(gg.spawnTimers[gg.spawnNumber] < gg.gameTimer) {		//it is past the time to spawn a group of enemies
				spawnEnemyGroup(gg);
				if(gg.spawnNumber % 3 == 0)						//every third group spawn tractor beam enemy
				{
					spawnTractorEnemy(gg);
				}
				gg.spawnNumber++;
			}
		}
			
		for(ArrayList<EnemyShip> A : gg.enemies) {
			for(EnemyShip e : A) {
				e.update(delta, gg.moveDirection);
			}
		}
		
		for(TractorBeamEnemy t : gg.tractorEnemy) {
			t.update(delta, gg.moveDirection, null);
		}
		
		for(EnemyBullet eb : gg.enemyBullets) {
			eb.update(delta);
		}
		for (Iterator<Explosion> i = gg.explosions.iterator(); i.hasNext();) {
			if (!i.next().isActive()) {
				i.remove();
			}
		}

	}
	

	void spawnEnemyGroup(GalagaGame gg) {
		ArrayList<EnemyShip> ship = new ArrayList<EnemyShip>();
		
		for(int i = 0; i <=6; i++) {
			ship.add(new EnemyShip
					(gg.sp.get(gg.spawnNumber).getX(), gg.sp.get(gg.spawnNumber).getY(), 
							0f, 0.6f, gg.groupID, i, gg.sp.get(gg.spawnNumber).flyToPoints));
		}
		gg.enemies.add(ship);
		gg.groupID++;
	}
	
	void spawnTractorEnemy(GalagaGame gg) {
		gg.tractorEnemy.add(new TractorBeamEnemy(GalagaGame.screenwidth/2, -TractorBeamEnemy.tractorEnemyHeight, 0f, 0f, gg.tractorColumn, gg.tractorRow, 3000));
		gg.tractorColumn++;
		if(gg.tractorColumn > 3) {
			gg.tractorColumn = 0;
			gg.tractorRow++;
		}
	}

	@Override
	public int getID() {
		return GalagaGame.DEADSTATE;
	}
	
}
