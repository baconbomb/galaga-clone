package Galaga;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;

public class MenuState extends BasicGameState {

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		Background b = new Background(GalagaGame.screenwidth/2, GalagaGame.screenheight/2);
		gg.bg.add(b);
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) {

	}
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		for(Background b : gg.bg) {
			b.render(g);
		}
		
		g.drawImage(ResourceManager.getImage(GalagaGame.GALAGA_TITLE_RSC),
				GalagaGame.screenwidth/2 - 300, 200);
		
		g.scale(2, 2);
		g.setColor(Color.green);
		g.drawString("Press Enter", 170, 200);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

		GalagaGame gg = (GalagaGame)game;
		
		Input input = container.getInput();
		
		if(input.isKeyDown(Input.KEY_ENTER)) {
			gg.enterState(GalagaGame.LEVELSTATE);		//start the game
		}
	}

	@Override
	public int getID() {
		return GalagaGame.MENUSTATE;
	}

	
}
