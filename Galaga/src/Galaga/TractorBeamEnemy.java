package Galaga;

import java.util.concurrent.ThreadLocalRandom;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class TractorBeamEnemy extends Entity {
	
	Vector velocity;
	
	static int tractorEnemyHeight = 50;
	static int tractorEnemyWidth = 50;
	
	boolean hit = false;			//ship needs to hits to die
	
	//keep track of state of the ship
	boolean startBeam = false;		
	boolean hasBeam = false;
	boolean attacking = false;
	boolean atFinalPos = false;
	boolean shipGrabbed = false;
	
	int column = 3;
	int groupIDnum = 1;
	
	int beamTimer = 0;		//time the beam is active for
	int grabTimer = 0;		//time the player is in the beam
	int attackSetTimer = 3000;
	int attackTimer = 3000;			//ms to wait before trying to grab the player
	int flyAwayTimer = ThreadLocalRandom.current().nextInt(0, 10000);	//randomize time for ship to try to run with after grabbing player ship
	

	public TractorBeamEnemy(final float x, final float y, final float vx, final float vy, 
			int tractorBeamColumn, int tractorBeamRow, int attackingTime){
		super(x,y);
		velocity = new Vector(vx, vy);
		column = (tractorBeamColumn * 100) + 400;
		groupIDnum = (tractorBeamRow * 100) + 100;
		attackSetTimer = attackingTime;
		setATimer();
		addImageWithBoundingBox(ResourceManager.getImage(GalagaGame.ENEMY_TRAC_RSC));
	}
	
	void setATimer() {
		attackTimer = attackSetTimer;
	}
	
	void removeBeam() {			//remove the tractor beam image
		if(hasBeam) {
			hasBeam = false;
			removeImage(ResourceManager.getImage(GalagaGame.TRACTOR_BEAM_RSC));
		}
	}
	
	void zeroTimer() {
		grabTimer = 0;
		beamTimer = 0;
	}
	void setAttack() {
		attacking = true;
	}
	
	void notAttack() {
		attacking = false;
	}
	
	//same as normal enemies
	void flyToFinalPos(int moveDirection) {
		
		removeBeam();		//remove tractor beam if not needed
		
		Vector finalPos = new Vector(column + moveDirection, groupIDnum);
		Vector currentPos = this.getPosition();
			
		if((Math.abs(
				finalPos.getX() - currentPos.getX()) < 15) && Math.abs(finalPos.getY() - currentPos.getY()) < 15) { 
			atFinalPos = true;
			return;
		}
			
		flyTowards(currentPos, finalPos);
	}
	
	void update(int delta, int moveDirection, Ship ship) {
		translate(velocity.scale(delta));
		
		if(!atFinalPos) { //go to final pos
			flyToFinalPos(moveDirection);
			}
		else if (!attacking && atFinalPos && !shipGrabbed){	//at final pos and not read to do anything else
			velocity = new Vector(0f, 0f);
			this.setX(column + moveDirection);	//step with large group
			setY(groupIDnum);
		}
		if(attacking && !shipGrabbed) {		//no shipped grabbed time to attack
			if(ship == null) {				//no player ship, ship is dead
				attacking = false;
				atFinalPos = false;			//fly back to final pos
				startBeam = false;
				grabTimer = 0;
				beamTimer = 0;
			}
			else {
				if(startBeam) {			//close enough to ship to try to grab
					if(hasBeam == false) {
						addImage(ResourceManager.getImage(GalagaGame.TRACTOR_BEAM_RSC), new Vector(0, 120));
						hasBeam = true;
					}
					doBeam(ship, delta);	//continue trying to grab
				}
				else{					//not close enough to grab
					flyTowards(this.getPosition(), new Vector(ship.getX(), ship.getY()));	//fly towards player ship	
					if(GalagaGame.screenheight - 60 - this.getY() < 200) {
						startBeam = true;			//start the beam
					}
				}
			}
		}
		else if(!shipGrabbed){		//hasnt grabbed ship yet
			attackTimer -= delta;
			if(attackTimer < 0) {	//ship is grabbed
				setATimer();		
				attacking = true;
			}
		}
		else if(shipGrabbed){		//ship was grabbed
			if(this.getNumImages() == 1) {
				addImage(ResourceManager.getImage(GalagaGame.PLAYER_SHIP_CAPTURED_RSC), new Vector(0, -60));
				atFinalPos = false;
				attacking = false;
			}
			if(!atFinalPos) {					//fly back to final pos
				flyToFinalPos(moveDirection);
			}
			else{
				endFlight(moveDirection, delta);	//trying to run away with player ship
			}
		}
	}
	
	void endFlight(int moveDirection, int delta) {
		flyToFinalPos(moveDirection);
		if(flyAwayTimer < 0) {
			velocity = new Vector(0f, .6f);		//fly straight down away
		}
		else 
		{
			velocity = new Vector(0f, 0f);		//step with large group
			this.setX(column + moveDirection);
			setY(groupIDnum);
			flyAwayTimer -= delta;
		}
	}
	
	void doBeam(Ship ship, int delta) {
		velocity = new Vector(0f, 0f);
		beamTimer += delta;
		if(Math.abs(this.getX() - ship.getX()) < 100) {	//player must be with in 100 x units of the enemy to be grabbed
			grabTimer += delta;		//if playing is with in start counting for how long
		}
		else {
			grabTimer = 0;		//player no longer in tractor beam
		}
		if(grabTimer > 1000) {		//in the beam for 1000ms
			zeroTimer();
			ship.grabbed = true;	//grab the ship
			removeBeam();
			shipGrabbed = true;
			startBeam = false;
		}
		if(beamTimer > 2000) {	//didnt grab anything after 2000ms
			atFinalPos = false;
			attacking = false;
			zeroTimer();
			removeBeam();
			startBeam = false;
		}
	}
	
	//same as normal enemy ship
	//scaled vector in the direction of its next point
	void flyTowards(Vector currentPos, Vector nextPos) {
		Vector dir = new Vector(nextPos.subtract(currentPos));
		
		dir = dir.scale(.0005f);
		velocity = velocity.add(dir);
		
		velocity = velocity.scale(6/(10 * 
				((float)Math.sqrt((
						(double)velocity.getX() * velocity.getX()) + ((double)velocity.getY() * velocity.getY())))));
	}
	
	//test if ship should be destroyed or if its the first hit
	void shipHit() {
		if(!hit) {
			addImage(ResourceManager.getImage(GalagaGame.ENEMY_TRAC_HIT_RSC));
			removeImage(ResourceManager.getImage(GalagaGame.ENEMY_TRAC_RSC));			
		}
		hit = true;
	}
}
