package Galaga;


import java.util.ArrayList;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class EnemyShip extends Entity{
	
	static int eHeight = 33;
	static int eWidth = 40;
	int column;
	int tOffset;
	int groupIDnum;
	
	boolean offScreen = false;
	boolean xFinalPos = false;
	boolean yFinalPos = false;
	
	ArrayList<Vector> point;
	
	Vector velocity;
	
	double degree = 0;
	
	public EnemyShip(final float x, final float y, final float vx, final float vy, int groupID, int timerOffset, ArrayList<Vector> flyToPoint) {
		super(x,y);
		velocity = new Vector(vx, vy);
		addImageWithBoundingBox(ResourceManager.getImage(GalagaGame.ENEMY_SHIP_RSC));
		groupIDnum = groupID;				//row the ship belongs in
		tOffset = timerOffset * 150;		//how many ms after spawning should the ship wait before flying in
		column = timerOffset;				//col the ship belongs in
		point = new ArrayList<Vector>();	//points on its path to follow
		point.addAll(0, flyToPoint);		//add all the points 
		}
	
	//ship has been to all the points, now needs to end up in the large group
	void flyToFinalPos(int moveDirection) {
		Vector finalPos = new Vector((column * 80) + 200 + moveDirection, (groupIDnum * 60) + 100);	//using this ships row and col assigned earlier, determine the vector that points to that location
		Vector currentPos = this.getPosition();
				
		if((Math.abs(finalPos.getX() - currentPos.getX()) < 15) && Math.abs(finalPos.getY() - currentPos.getY()) < 15) { //is it close enough to its final pos
			xFinalPos = true;
			yFinalPos = true;
		}
		
		if(xFinalPos && yFinalPos) {
			endState(moveDirection);		//at final pos, instead of playing, step left and right with large group
			return;
		}
		flyTowards(currentPos, finalPos);	//flyrtowards final pos
	}
	
	//changes the velocity of the ship to point to the next point it is suppose to go to
	//does it slowly over time to make curves instead of sharp edges
	//points the ship in the right direction and maintains consistent speed
	void flyTowards(Vector currentPos, Vector nextPos) {
		Vector dir = new Vector(nextPos.subtract(currentPos));	//vector points from the ship to the next point
	
		dir = dir.scale(.0005f);			//scale the vector really small
		velocity = velocity.add(dir);		//add to current velocity
		
		//scale velocity to a unit vector * 6/10 the speed we want the ship to fly
		velocity = velocity.scale(6/(10 * ((float)Math.sqrt(((double)velocity.getX() * velocity.getX()) + ((double)velocity.getY() * velocity.getY())))));
	}
	
	//ship at final pos
	void endState(int moveDirection) {
		velocity = new Vector(0f, 0f);			//no velocity
		this.setX((column * 80) + 200 + moveDirection);	//move ship left and right with goup
		setY(groupIDnum * 60 + 100);
	}
	
	void update(int delta, int moveDirection) {
		if(!offScreen) {			//offscreen is true when there are no more points to fly to
			if(point.isEmpty()) {
				offScreen =true;
			}
			else if((Math.abs(point.get(0).getX() - this.getX()) < 90) && Math.abs(point.get(0).getY() - this.getY()) < 90) {
				point.remove(0);		//remove the first point on the list when it is close enough
			}
			else flyTowards(this.getPosition(), point.get(0));	//keep trying to fly towards the next point
			
		}
		if(offScreen) {		//no more points to fly to
			if(xFinalPos && yFinalPos) {	//at final pos
				endState(moveDirection);	//step with the group
				return;
			}
			flyToFinalPos(moveDirection);		//fly to final pos
			translate(velocity.scale(delta));	
			return;
		}
		
		tOffset = tOffset - delta;				//wait, this offesets the ships in the group

		if(tOffset < 0) {	
			translate(velocity.scale(delta));
		}
	}
}
