package Galaga;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;
import jig.Vector;

/*
 * The state of the game where the player has control of the ship 
 * and destroys the enemy.
 * 
 */


public class PlayingState extends BasicGameState{

	int groupID = 1;
	boolean collision = false;			//did the ship collide with somthing
		
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
	
		gg.enemies = new ArrayList<ArrayList<EnemyShip>>();		
		gg.tractorEnemy = new ArrayList<TractorBeamEnemy>();
		gg.lives = 3;
		gg.ship = new ArrayList<Ship>();
		gg.ship.add(new Ship(GalagaGame.screenwidth / 2, GalagaGame.screenheight - 50, 0f, 0f));
	}
	
	/*
	 * Render steps through all the lists of player ships and bullets
	 * and enemy ships and bullets and renders them
	 */
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		for(Background b : gg.bg) {
			b.render(g);
		}
		
		for(ArrayList<EnemyShip> ES : gg.enemies) {
			for(EnemyShip e : ES) {
				if(!e.xFinalPos) {
					gg.rotate =  e.velocity.getRotation() - 90;			//render the ship pointing the direction it is flying
					g.rotate(e.getX(), e.getY(), (float) gg.rotate);
					e.render(g);
					g.rotate(e.getX(), e.getY(), (float) -gg.rotate);	//rotate the graphics object back
				}
				else {
					e.render(g);
				}

			}
		}
		for(TractorBeamEnemy t : gg.tractorEnemy) {
			t.render(g);
		}
		for(Ship s : gg.ship) {
			s.render(g);
			if(!s.bullets.isEmpty()) {
				for(bullet b : s.bullets) {
					b.render(g);
				}
			}
		}	
		for(EnemyBullet eb : gg.enemyBullets) {
				gg.rotate =  eb.velocity.getRotation() - 90;
				g.rotate(eb.getX(), eb.getY(), (float) gg.rotate);	//render the bullet pointing the direction of flight
				eb.render(g);
				g.rotate(eb.getX(), eb.getY(), (float) -gg.rotate);	//rotate graphics object back
		}
		
		for(Explosion e : gg.explosions) {
			e.render(g);
		}
		
		g.scale(2, 2);
		g.setColor(Color.red);
		g.drawString("Lives " + gg.lives, 10, 10);
		g.drawString("Score : " + gg.score, 170, 10);
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		GalagaGame gg = (GalagaGame) game;
		
		for(Ship s : gg.ship) {
			s.bullets = new ArrayList<bullet>();	//remove any bullets left over 
		}
		
		gg.enemyBullets = new ArrayList<EnemyBullet>();	//remove left over bullets

		gg.enemyShootAtThisTime = ThreadLocalRandom.current().nextInt(0, 2500);		//randomize when the enemy should shoot
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		GalagaGame gg = (GalagaGame)game;
		
		if(gg.lives == 0) {
			game.enterState(GalagaGame.GAMEOVERSTATE);		//lost the game
		}
		
		gg.gameTimer = gg.gameTimer + delta;			//keep track of different timers 
		gg.moveTimer = gg.moveTimer + delta;	
		gg.enemyShootTimer = gg.enemyShootTimer + delta;
		
		Input input = container.getInput();
		
		if(input.isKeyDown(Input.KEY_A) || input.isKeyDown(Input.KEY_LEFT)) {	
			for(Ship s : gg.ship) {
				s.moveLeft(gg.ship.size(), gg.ship.indexOf(s));
			}	
		}
		if(input.isKeyDown(Input.KEY_D) || input.isKeyDown(Input.KEY_RIGHT)) {
			for(Ship s : gg.ship) {
				s.moveRight(gg.ship.size(), gg.ship.indexOf(s));
			}	
		}
		if(input.isKeyPressed(Input.KEY_SPACE)){
			for(Ship s : gg.ship) {			
				s.shoot(s.getX(), s.getY() - s.shipHeight, 0f, -1f);
			}	
		}
		
		if(gg.ship.get(0).grabbed) {		//ship grabbed by tractor beam enemy
			shipDied(gg);
		}
	
		if(gg.moveTimer > 500) {			//count up to .5 seconds
			gg.moveTimer = 0;
			gg.moveDirection = gg.moveDirection + gg.moveAmount;	//offset large group of enemys 
			if(gg.moveDirection > 30) {
				gg.moveAmount = gg.moveAmount * -1;
			}
			if(gg.moveDirection < 0) {
				gg.moveAmount = gg.moveAmount * -1;
			}
		}
		
		if(gg.spawnNumber < gg.spawnTimers.length ) {				//still enemies to spaw
			if(gg.spawnTimers[gg.spawnNumber] < gg.gameTimer) {		//it is past the time to spawn a group of enemies
				spawnEnemyGroup(gg);
				if(gg.spawnNumber % 3 == 0)						//every third group spawn tractor beam enemy
				{
					spawnTractorEnemy(gg);
				}
				gg.spawnNumber++;
			}
		}
		else if(gg.spawnNumber >= gg.spawnTimers.length) {				//no more enemies to spawn
			if(gg.enemies.isEmpty() && gg.tractorEnemy.isEmpty()) {		//no more enemies
				gg.level++;
				gg.enterState(GalagaGame.LEVELSTATE);					//make next level
			}		
		}
		
		if(gg.enemyShootTimer > gg.enemyShootAtThisTime) {				//count up until time for enemies to shoot
			if (!gg.enemies.isEmpty()) {
					int col = ThreadLocalRandom.current().nextInt(0, gg.enemies.size());	//randomize who shoots
					if(col > 0) {
						col = col - 1;
					}
					int row = ThreadLocalRandom.current().nextInt(0, gg.enemies.get(col).size());
					if(row > 0) {
						row = row - 1;
					}
					if(gg.enemies.get(col).get(row) != null) {
						EnemyShoots(gg.enemies.get(col).get(row).getPosition(), gg);	//shoot at the ship
					}
			}
			gg.enemyShootAtThisTime = ThreadLocalRandom.current().nextInt(1000, 2500);		//randomize time until next shot, always less then 2.5 sec
			gg.enemyShootTimer = 0;
		}
		
		for(Ship s : gg.ship) {							//update pos of all player bullets
			if(!s.bullets.isEmpty()) {
				s.update(delta);
		 	}		
		}
			
		for(ArrayList<EnemyShip> A : gg.enemies) {		//update pos of all enemy ships
			for(EnemyShip e : A) {
				e.update(delta, gg.moveDirection);
			}
		}
		
		for(EnemyBullet eb : gg.enemyBullets) {			//update pos of all enemy bullets
			eb.update(delta);
		}
		
		collision = shipHit(gg);					//did the ship get him by somthing
		
		if(collision) {								//ship was hit
			collision = false;
			shipDied(gg);							//test to see if the ship is dead, or just need to remove the second ship
		}
		
		
		for(TractorBeamEnemy t : gg.tractorEnemy) {		//update pos of tractor beam enemies
			t.update(delta, gg.moveDirection, gg.ship.get(0));
		}
		
		bulletHit(gg, delta);						//did player bullet hit anything
		removeEnemyGroup(gg);						//since we have a list of list we need to remove empty lists from the big list
													//if there are no more enemies on a list remove the list
		
		for (Iterator<Explosion> i = gg.explosions.iterator(); i.hasNext();) {
			if (!i.next().isActive()) {				//remove old animations for explosions
				i.remove();
			}
		}
		
	}
	
	//test to see if the ship was hit by enemy bullets or ships
	boolean shipHit(GalagaGame gg) {
		for(Ship s : gg.ship) {
			for(EnemyBullet eb : gg.enemyBullets) {
				if(s.collides(eb) != null) {
					gg.explosions.add(new Explosion(s.getX(), s.getY()));
					gg.enemyBullets.remove(eb);
					return true;
				}
			}
			for(ArrayList<EnemyShip> es : gg.enemies) {
				for(EnemyShip ship : es) {
					if(s.collides(ship) != null) {
						gg.explosions.add(new Explosion(ship.getX(), ship.getY()));
						return true;
						}
				}
			}
			for(TractorBeamEnemy tb : gg.tractorEnemy) {
				if(s.collides(tb) != null) {
					gg.explosions.add(new Explosion(s.getX(), s.getY()));
					return true;
				}
			}
		}
		return false;
	}
	
	//enemy bullet flew off screen, remove from game
	
	void removeEnemyBullet(GalagaGame gg) {
		EnemyBullet rBullet = null;
		
		for(EnemyBullet eb : gg.enemyBullets) {
			if (eb.getY() > GalagaGame.screenheight) {	//bullet off screen
				rBullet = eb;
			}
		}
		gg.enemyBullets.remove(rBullet);
	}
	
	//enemy shoots at player ship
	
	void EnemyShoots(Vector v, GalagaGame gg){
		Vector dir = new Vector(gg.ship.get(0).getX() - v.getX(), gg.ship.get(0).getY() - v.getY());
		EnemyBullet eb = new EnemyBullet(v.getX(), v.getY(),  dir.getX(), dir.getY());
		gg.enemyBullets.add(eb);
		ResourceManager.getSound(GalagaGame.ENEMY_LASER_S_RSC).play();
	}
	
	//enemy list is empty, remove from the large list of enemies
	void removeEnemyGroup(GalagaGame gg) {
		ArrayList<EnemyShip> removeGroup = null;
		for(ArrayList<EnemyShip> e : gg.enemies) {
			if (e.isEmpty()) {
				removeGroup = e;
			}
		}
		if(removeGroup != null) {
			gg.enemies.remove(removeGroup);
		}
	}
	
	//player ship was hit, remove second ship or destroy player ship
	void shipDied(GalagaGame gg) {
		if(gg.ship.size() == 1) {
			gg.enterState(GalagaGame.DEADSTATE);
		}
		else {
			gg.ship.remove(gg.ship.size() - 1);
		}
	}
	
	//spawn tractor beam enemy
	void spawnTractorEnemy(GalagaGame gg) {
		gg.tractorEnemy.add(new TractorBeamEnemy(GalagaGame.screenwidth/2, -TractorBeamEnemy.tractorEnemyHeight, 0f, 0f, gg.tractorColumn, gg.tractorRow, 3000));
		gg.tractorColumn++;
		if(gg.tractorColumn > 3) {
			gg.tractorColumn = 0;
			gg.tractorRow++;
		}
	}
	
	//spawn group of normal enemies
	void spawnEnemyGroup(GalagaGame gg) {
		ArrayList<EnemyShip> ship = new ArrayList<EnemyShip>();
		
		for(int i = 0; i <=6; i++) {
			ship.add(new EnemyShip
					(gg.sp.get(gg.spawnNumber).getX(), gg.sp.get(gg.spawnNumber).getY(), 
							0f, 0.6f, gg.groupID, i, gg.sp.get(gg.spawnNumber).flyToPoints));
		}
		gg.enemies.add(ship);
		gg.groupID++;
	}

	//somthing was destroyed add to points
	void addScore(EnemyShip es, TractorBeamEnemy et, GalagaGame gg) {
		if(es != null) {
			if(es.xFinalPos){
				gg.score += 50;			//normal enemy 50 while at final pos
			}
			else {
				gg.score += 100;		//normal enemy 100 while still flying around
			}
		}
		else if(et != null) {
			if(et.atFinalPos == true){	//tractor beam enemy 80 while in group
				gg.score += 80;
			}
			else if(!et.atFinalPos && et.shipGrabbed) {
				gg.score += 1000;		//1000 points if its in the middle of stealing a ship
			}
			else {
				gg.score += 160;		//tractor beam enemy 160 if still flying
			}
		}
	}
	
	//test if player bullets hit an enemy
	void bulletHit(GalagaGame gg, int delta) {
		ArrayList<EnemyShip> rEnemy = new ArrayList<EnemyShip>();
		TractorBeamEnemy rTEnemy = null;
		
		bullet rBullet = null;
		Ship rShip = null;
		
		for(Ship s : gg.ship) {					//loop through all bullets and all enemies
			for(bullet b : s.bullets) {
				for(ArrayList<EnemyShip> e : gg.enemies) {
					for(EnemyShip ship : e) {
						if(b.collides(ship) != null) {
							rEnemy.add(ship);
							rBullet = b;			//store what needs to be remove for later
							rShip = s;				//othwise will mess up iteration
							addScore(rEnemy.get(0), null, gg);
							gg.explosions.add(new Explosion(ship.getX(), ship.getY()));
						}
					}
				}
				for(TractorBeamEnemy t : gg.tractorEnemy) {
					if(b.collides(t) != null) {
						gg.explosions.add(new Explosion(t.getX(), t.getY()));
						if(t.hit) {
							rTEnemy = t;
							addScore(null, rTEnemy, gg);
						}
						t.shipHit();
						rBullet = b;
						rShip = s;
					}
					if(t.getY() > GalagaGame.screenheight) {
						rTEnemy = t;						
						rTEnemy.shipGrabbed = false;
					}
				}
			}
		}
		
		if(rBullet != null) {					//remove if something was hit
			rShip.bullets.remove(rBullet);
		}
		if(rEnemy != null) {
			for(ArrayList<EnemyShip> ES : gg.enemies) {
				ES.removeAll(rEnemy);
			}
		}
		if(rTEnemy != null) {
			gg.tractorEnemy.remove(rTEnemy);
			if(rTEnemy.shipGrabbed == true) {
				addShip(gg);
			}
		}
	}
	
	//double ship power up
	void addShip(GalagaGame gg) {
		if(gg.ship.size() < 2) {
			gg.ship.add(new Ship(gg.ship.get(0).getX() + gg.ship.get(0).shipWidth, GalagaGame.screenheight - 50, 0f, 0f));
		}
	}

	@Override
	public int getID() {
		return GalagaGame.PLAYINGSTATE;
	}
	

}
