package Galaga;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.Vector;

public class LevelState extends BasicGameState{

	
	/*
	 * Make levels for the player to play
	 */
	int levelScreenTimer = 0;
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		for(Background b : gg.bg) {
			b.render(g);
		}
		g.scale(2, 2);
		g.setColor(Color.green);
		g.drawString("Level: " + gg.level, 170, 200);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		GalagaGame gg = (GalagaGame) game;
		
		setToZero(gg);
		
		switch(gg.level) {
		case 1: MakeLevel1(gg);		//make level 1
				break;
			
		case 2: MakeLevel2(gg);		//make level 2
				break;
				
		default: MakeRandomLevel(gg);	//levels after 2 are random with increasing difficulty
	
		}
		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		levelScreenTimer += delta;
		
		if(levelScreenTimer > 2000) {		//time to display the level number
			levelScreenTimer = 0;
			gg.enterState(GalagaGame.PLAYINGSTATE);
		}
	}

	@Override
	public int getID() {
		return GalagaGame.LEVELSTATE;
	}
	
	//make random level
	//has level + 4 groups of normal enemies
	//enemies fly to level + 2 points
	void MakeRandomLevel(GalagaGame gg) {
		int time = 0;
		
		gg.spawnTimers = new int[gg.level + 4];
		
		for(int i = 0; i < gg.spawnTimers.length; i++) {
			gg.spawnTimers[i] = time;				//create list of times to spawn enemies
			time = time + 5000;
		}
		
		gg.sp = new ArrayList<EnemyPoint>();
		
		for(int i = 0; i < gg.spawnTimers.length; i++) {	//odd group numbers spawn on the right, even on the left
			if(i % 2 == 0) {
				gg.sp.add(new EnemyPoint(-100, (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenheight)));
			}
			else {
				gg.sp.add(new EnemyPoint(GalagaGame.screenwidth + 100, (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenheight)));
			}
		}

		float x;
		float y;
		for(EnemyPoint p : gg.sp) {
			p.flyToPoints = new ArrayList<Vector>();

			for(int i = 0; i < gg.spawnTimers.length - 1; i++) {
				x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
				y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
				p.flyToPoints.add(new Vector(x, y));			//randomize points to fly to
																//points are anything on x
																//300 to end of screen on y
			}
			p.flyToPoints.add(new Vector(GalagaGame.screenwidth/2, -200));	//last point is off the top of the screen
		}
	}
	
	void setToZero(GalagaGame gg) {		//restart the timers for each level
		gg.spawnNumber = 0;
		gg.groupID = 1;
		
		gg.gameTimer = 0; 
		gg.moveTimer = 0;
		gg.respawnTimer = 0;
		
		gg.moveDirection = 0;
		gg.moveAmount = 2;
		
		gg.tractorColumn = 0;
		gg.tractorRow = 0;
	}
	
	//level 1 
	//5 groups of enemies
	//fly to 2 points before going to final pos
	void MakeLevel1(GalagaGame gg) {
		int time = 0;
		
		gg.spawnTimers = new int[5];
		
		for(int i = 0; i < gg.spawnTimers.length; i++) {
			gg.spawnTimers[i] = time;
			time = time + 5000;
		}
		
		gg.sp = new ArrayList<EnemyPoint>();
		gg.sp.add(new EnemyPoint(200, -100));
		gg.sp.add(new EnemyPoint(GalagaGame.screenwidth - 200, -100));
		gg.sp.add(new EnemyPoint(-100, 300));
		gg.sp.add(new EnemyPoint(GalagaGame.screenwidth + 100, 300));
		gg.sp.add(new EnemyPoint(-100, 0));
		
		float x;
		float y;
		for(EnemyPoint p : gg.sp) {
			x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
			y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
			p.flyToPoints = new ArrayList<Vector>();
			p.flyToPoints.add(new Vector(x, y));
			x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
			y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
			p.flyToPoints.add(new Vector(x, y));
			p.flyToPoints.add(new Vector(GalagaGame.screenwidth/2, -200));
		}
	}
	
	//level 2
	//make 6 groups of enemies
	//fly to 3 points before flying to final pos
	void MakeLevel2(GalagaGame gg) {
		int time = 0;
		
		gg.spawnTimers = new int[6];
		
		for(int i = 0; i < gg.spawnTimers.length; i++) {
			gg.spawnTimers[i] = time;
			time = time + 5000;
		}
		
		gg.sp = new ArrayList<EnemyPoint>();
		gg.sp.add(new EnemyPoint(200, -100));
		gg.sp.add(new EnemyPoint(GalagaGame.screenwidth - 200, -100));
		gg.sp.add(new EnemyPoint(-100, 300));
		gg.sp.add(new EnemyPoint(GalagaGame.screenwidth + 100, 300));
		gg.sp.add(new EnemyPoint(-100, 0));
		gg.sp.add(new EnemyPoint(-100, GalagaGame.screenwidth));
		
		float x;
		float y;
		for(EnemyPoint p : gg.sp) {
			x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
			y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
			p.flyToPoints = new ArrayList<Vector>();
			p.flyToPoints.add(new Vector(x, y));
			x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
			y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
			p.flyToPoints.add(new Vector(x, y));
			x = (float)ThreadLocalRandom.current().nextInt(0, GalagaGame.screenwidth);
			y = (float)ThreadLocalRandom.current().nextInt(300, GalagaGame.screenheight - 90);
			p.flyToPoints = new ArrayList<Vector>();
			p.flyToPoints.add(new Vector(x, y));
			p.flyToPoints.add(new Vector(GalagaGame.screenwidth/2, -200));
			
		}
	}

}
