package Galaga;


import java.util.ArrayList;

import jig.Entity;
import jig.ResourceManager;

public class Ship extends Entity{
	
	int movespeed =  8;			//speed the ship can move left or right

	int shipWidth = 50;
	int shipHeight = 52;
	int screenWidth = 840;
	
	boolean dead = false;		//ship is dead
	boolean grabbed = false;	//ship grabbed by tractor beam
	
	ArrayList<bullet> bullets;
	
	public Ship(final float x, final float y, final float vx, final float vy) {
		super(x, y);
		addImageWithBoundingBox(ResourceManager
				.getImage(GalagaGame.PLAYERS_SHIP_RSC));
		bullets = new ArrayList<bullet>();		//list of bullets shot by the ship
	}
	
	void shipDied() {
		dead = true;
		//setX(-100);
	}
	
	void shipAlive() {
		dead = false;
		setX(GalagaGame.screenwidth / 2);
	}

	void moveLeft(int size, int thisShip) {
		if(size == 1) {				//normal case 1 ship
			if(super.getX() - shipWidth > 0){
				super.setX(super.getX() - movespeed);
			}
		}
		else if(size == 2) {		//special case double ship power up
			if(thisShip == 0) {
				if(super.getX() - shipWidth > 0){	//move left ship all the way left
					super.setX(super.getX() - movespeed);
				}
			}
			else {
				if(super.getX() - shipWidth - shipWidth > 0){	//move right ship shipWidth away from all the way left
					super.setX(super.getX() - movespeed);
				}
			}
		}		
	}
	
	void moveRight(int size, int thisShip) {
		if(size == 1) {
			if(super.getX() + shipWidth < screenWidth) {
				super.setX(super.getX() + movespeed);
			}
		}
		else if (size == 2) {
			if(thisShip == 0) {
				if(super.getX() + shipWidth + shipWidth< screenWidth) {
					super.setX(super.getX() + movespeed);
				}
			}
			else {
				if(super.getX() + shipWidth < screenWidth) {
					super.setX(super.getX() + movespeed);
				}
			}
		}
	}
	
	void shoot(float x, float y, float vx, float vy) {
		if(bullets.size() < 2) {		//only two bullets at a time
			ResourceManager.getSound(GalagaGame.PLAYER_LASER_S_RSC).play();
			bullets.add(new bullet(x, y, vx, vy));
		}
	}
	
	void update(int delta) {
		bullet toRemove = null;
		for(bullet b : bullets) {
			b.update(delta);
			if(b.getY() < 0 - b.bHeight) {	//bullet offscreen
				toRemove = b;
			}
		}	
		if(toRemove != null) {
			bullets.remove(toRemove);		//remove bullet
		}
	}

}
