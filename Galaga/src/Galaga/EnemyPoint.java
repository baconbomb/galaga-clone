package Galaga;

import java.util.ArrayList;

import jig.Vector;

public class EnemyPoint {	//points for enemy ship

	float x;		//spawn at this x
	float y;		//spawn at this y
	ArrayList<Vector> flyToPoints;	//fly to this list of points, how the flight path is made
	
	public EnemyPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	float getX() {
		return x;
	}
	
	float getY() {
		return y;
	}
}
