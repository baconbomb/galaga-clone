package Galaga;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import jig.Entity;
import jig.ResourceManager;

/* Galaga Clone game 
 * Author Spencer Bacon
 */

/* Art found at 
 * https://opengameart.org/content/space-starter-kit 
 *
 * https://opengameart.org/content/vector-spaceship
 * 
 * https://opengameart.org/content/laser-fire
 * https://opengameart.org/content/boom-pack-1
 * */

public class GalagaGame extends StateBasedGame{

	public static final int MENUSTATE = 0;
	public static final int PLAYINGSTATE = 1;
	public static final int DEADSTATE = 2;
	public static final int LEVELSTATE = 3;
	public static final int GAMEOVERSTATE = 4;
	
	public static final int screenwidth = 840;
	public static final int screenheight = 1008;
	
	public static final String PLAYERS_SHIP_RSC = "starship.png";
	public static final String PLAYER_BULLET_RSC = "playerbullet.png";
	public static final String ENEMY_SHIP_RSC = "enemy.png";
	public static final String ENEMY_TRAC_RSC = "tractorenemy.png";
	public static final String ENEMY_TRAC_HIT_RSC = "tractorenemyhit.png";
	public static final String TRACTOR_BEAM_RSC = "tractor2.png";
	public static final String PLAYER_SHIP_CAPTURED_RSC = "starshipcaptured.png";
	public static final String ENEMY_BULLET_RSC = "enemybullet.png";
	public static final String EXPLOSION_RSC = "explosion.png";
	public static final String EXPLOSION_S_RSC = "explosion.wav";
	public static final String PLAYER_LASER_S_RSC = "playerlaser.wav";
	public static final String ENEMY_LASER_S_RSC = "enemylaser.wav";
	public static final String BACKGROUND_RSC = "background.png";
	public static final String GALAGA_TITLE_RSC = "GalagaTitle.png";

	ArrayList<Ship> ship;		//list of players ship
	
	ArrayList<ArrayList<EnemyShip>> enemies;	//list of groups of normal enemy ships
	ArrayList<TractorBeamEnemy> tractorEnemy;	//list of tractor beam enemies that can grab the player
	ArrayList<EnemyBullet> enemyBullets;		//list of bullets enemies have shot at player
	
	ArrayList<Explosion> explosions;			//list of animations for explosions
	
	ArrayList<Background> bg;					//animated background
	
	int[] spawnTimers;							//list of ms into a level that enemies should spawn at
	ArrayList<EnemyPoint> sp;					//list of spawn points enemies spawn at for a level
	int spawnNumber;							//number of groups of enemies that are going to spawn
	int groupID = 1;							//groupID assigns columns to the enemies for their final position in the large group
	
	int lives;									//player lives count
	int level = 1;								//level player is on
	
	int gameTimer = 0; 							//total ms a level has been played for
	int moveTimer = 0;							//ms until the large group should move left or right
	int respawnTimer = 0;						//keeps track when the player ship should respawn after being dead
	int enemyShootTimer = 0;					//ms since last enemyshot
	int enemyShootAtThisTime;					//random ms when an enemy should shoot the player
	
	int moveDirection = 0;						//x off set for large group
	int moveAmount = 2;							//amount to move for each step
	
	int tractorColumn = 0;						//tractor beam large group assignment
	int tractorRow = 0;
	
	int score;									//players score
	
	double rotate;
		
	public GalagaGame(String title, int width, int height) {
		super(title);
		
		Entity.setCoarseGrainedCollisionBoundary(Entity.AABB);
		
		explosions = new ArrayList<Explosion>(10);
		bg = new ArrayList<Background>(1);
	}
	
	public static void main(String[] args) {
		AppGameContainer app;
		try {
			app = new AppGameContainer(new GalagaGame("Galaga!", GalagaGame.screenwidth, GalagaGame.screenheight));
			app.setDisplayMode(GalagaGame.screenwidth, GalagaGame.screenheight, false);
			app.setVSync(true);
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		addState(new MenuState());
		addState(new LevelState());
		addState(new PlayingState());
		addState(new DeadState());
		addState(new GameOverState());
		
		ResourceManager.loadSound(EXPLOSION_S_RSC);
		ResourceManager.loadSound(PLAYER_LASER_S_RSC);
		ResourceManager.loadSound(ENEMY_LASER_S_RSC);
		
		ResourceManager.loadImage(PLAYERS_SHIP_RSC);
		ResourceManager.loadImage(PLAYER_BULLET_RSC);
		ResourceManager.loadImage(ENEMY_SHIP_RSC);
		ResourceManager.loadImage(ENEMY_TRAC_RSC);
		ResourceManager.loadImage(ENEMY_TRAC_HIT_RSC);
		ResourceManager.loadImage(TRACTOR_BEAM_RSC);
		ResourceManager.loadImage(PLAYER_SHIP_CAPTURED_RSC);
		ResourceManager.loadImage(ENEMY_BULLET_RSC);
		ResourceManager.loadImage(EXPLOSION_RSC);
		ResourceManager.loadImage(BACKGROUND_RSC);
		ResourceManager.loadImage(GALAGA_TITLE_RSC);

	}
}