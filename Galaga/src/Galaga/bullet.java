package Galaga;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class bullet extends Entity{

	int bHeight = 31;
	
	Vector velocity;
	
	public bullet(final float x, final float y, final float vx, final float vy) {	//players bullets
		super(x,y);
		velocity = new Vector(vx, vy);
		addImageWithBoundingBox(ResourceManager.getImage(GalagaGame.PLAYER_BULLET_RSC));
	}
	
	public void update(final int delta) {
		translate(velocity.scale(delta));
	}
	
}
