package Galaga;

import org.newdawn.slick.Animation;

import jig.Entity;
import jig.ResourceManager;

public class Background extends Entity{
	
	private Animation background;

	public Background(final float x, final float y) {	//animated stars flying in backgroud
		super(x, y);
		background = new Animation(ResourceManager.getSpriteSheet(
				GalagaGame.BACKGROUND_RSC, 1000, 1000), 0, 0, 6, 0, true, 500,
				true);
		addAnimation(background);
		background.setLooping(true);
	}
}
