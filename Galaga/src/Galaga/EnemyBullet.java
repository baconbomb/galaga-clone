package Galaga;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class EnemyBullet extends Entity{

Vector velocity;
	
	public EnemyBullet(final float x, final float y, final float vx, final float vy) { //enemy bullet class
		super(x,y);
		velocity = new Vector(vx, vy);
		velocity = velocity.scale(6/(10 * ((float)Math.sqrt(((double)velocity.getX() * velocity.getX()) + ((double)velocity.getY() * velocity.getY())))));
		addImageWithBoundingBox(ResourceManager.getImage(GalagaGame.ENEMY_BULLET_RSC));
	}
	
	public void update(final int delta) {
		translate(velocity.scale(delta));
	}
}
