package Galaga;

import org.newdawn.slick.Animation;

import jig.Entity;
import jig.ResourceManager;

class Explosion extends Entity{
	private Animation explosion;
	
	public Explosion(final float x, final float y) {
		super(x, y);
		explosion = new Animation(ResourceManager.getSpriteSheet(
				GalagaGame.EXPLOSION_RSC, 64, 64), 0, 0, 22, 0, true, 50,	//explosion animation
				true);
		addAnimation(explosion);
		explosion.setLooping(false);
		ResourceManager.getSound(GalagaGame.EXPLOSION_S_RSC).play();
	}
	
	public boolean isActive() {
		return !explosion.isStopped();
	}
}
