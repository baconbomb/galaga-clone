package Galaga;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameOverState extends BasicGameState{

	/*
	 * Gameoverstate
	 * no more lives
	 * end of game
	 */
	int timer = 0;
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {		
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		GalagaGame gg = (GalagaGame) game;

		for(Background b : gg.bg) {
			b.render(g);
		}
		g.scale(2, 2);
		g.setColor(Color.red);
		g.drawString("Game Over", 170, 200);			//display points
		g.drawString("Score: " + gg.score, 170, 220);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		GalagaGame gg = (GalagaGame) game;
		
		timer = timer + delta;
		
		if (timer > 3000) {
			gg.lives = 3;
			timer = 0;
			gg.score = 0;
			game.enterState(GalagaGame.MENUSTATE);
		}
		
	}
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		GalagaGame gg = (GalagaGame) game;
		gg.enemies = new ArrayList<ArrayList<EnemyShip>>();
		gg.tractorEnemy = new ArrayList<TractorBeamEnemy>();
		gg.lives = 3;
		gg.ship = new ArrayList<Ship>();
		gg.ship.add(new Ship(GalagaGame.screenwidth / 2, GalagaGame.screenheight - 50, 0f, 0f));
		gg.level = 1;
	}

	@Override
	public int getID() {
		return GalagaGame.GAMEOVERSTATE;
	}

}
