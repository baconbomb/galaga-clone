## Resolved Issues ##

 **[1]** Make an executable game 
 **[2]** Add ship with keybord controls
 **[3]** Add bullets for ship to shoot
 **[17]** Two bullets at a time
 **[4]** Add enemies 
 **[5]** Add path for groups of enemies
 **[7]** Add collision for bullets
 **[6]** Enemy final on screen position
 **[11]** Add player lives, finished final pos
 **[20]** Respawn ship on death
 **[8]** Add special tractor beam enemy
 **[9]** Add double ship power up
 **[18]** Double ship special cases
 **[19]** Add levels 
 **[12]** Add scoring
 **[21]** Enemy shooting
 **[14]** Add explosions
 **[15]** Add sound
 **[16]** Add animated background
 **[13]** Finish user hud

## Open Issues ##

 **[10]** Challenge level
